import dialogsReducer from "./dialogsReducer";
import friendsReducer from "./friendsReducer";
import profileReducer from "./profileReducer";

let store = {
  _state: {
    profilePage: {
      posts: [ 
        { id: 1, message: "It`s my first post.", likesCount: 12 },
        { id: 2, message: "What is your name?.", likesCount: 22 },
      ],
      newPostText: "M",
    },

    dialogsPage: {
      dialogs: [
        { id: 1, name: "Мохьмад" },
        { id: 2, name: "Мага" },
        { id: 3, name: "Усам" },
        { id: 4, name: "Хамзат" },  
        { id: 5, name: "Джон" },
      ],

      messages: [
        { id: 1, message: "Привет" },
        { id: 2, message: "Как дела?" },
        { id: 3, message: "Пока" },
        { id: 4, message: "Ты мёртв?" },
        { id: 5, message: "Есть задание!" },
      ],
    },
    friendsPage: {
      friends: [
        {
          id: 1,
          name: "John Wick",
          image:
            "https://i.pinimg.com/originals/f8/39/7f/f8397fa106e0cbb1c47becf65e935008.png",
        },
        {
          id: 2,
          name: "Peter Parker",
          image:
            "https://cdn.vox-cdn.com/thumbor/pTa_yD0mbfDWi7AeAOA7Ym7R4qE=/1400x1400/filters:format(jpeg)/cdn.vox-cdn.com/uploads/chorus_asset/file/23102001/DF_12080_rv2_1000x563_thumbnail.jpg",
        },
        {
          id: 3,
          name: "Henry Cavill",
          image: "https://overclockers.ru/st/legacy/blog/127193/189904_O.jpg",
        },
      ],
    },
  },
  _callSubscriber() {
    console.log("state changed");
  },

  getState() {
    return this._state;
  },
  subscribe(observer) {
    this._callSubscriber = observer;
  },

  addPost() {
    let newPost = {
      id: 5,
      message: this._state.profilePage.newPostText,
      likesCount: 0,
    };
    this._state.profilePage.posts.push(newPost);
    this._state.profilePage.newPostText = "";
    this._callSubscriber(this._state);
  },

  updateNewPostText(newText) {
    this._state.profilePage.newPostText = newText;
    this._callSubscriber(this._state);
  },

  dispatch(action) {
    this._state.profilePage = profileReducer(this._state.profilePage, action);
    this._state.dialogsPage = dialogsReducer(this._state.dialogsPage, action);
    this._state.friendsPage = friendsReducer(this._state.friendsPage, action);

    this._callSubscriber(this._state);
  },
};

export default store;
