import { combineReducers, createStore } from "redux"; //импортируем из Redux две функции: createStore и conbine reducers
import authReducer from "./authReducer";
import dialogsReducer from "./dialogsReducer";
import friendsReducer from "./friendsReducer";
import profileReducer from "./profileReducer";
import usersReducer from "./usersReducer";

let reducers = combineReducers({
  profilePage: profileReducer,
  dialogsPage: dialogsReducer,
  friendsPage: friendsReducer,
  usersPage: usersReducer,
  auth: authReducer,
}); //создали свой state, где за каждую ветку отвечает свой reducer

let store = createStore(reducers); //создали store при помощи функции из Redux createStore
//store хранит полное дерево состояния нашего приложения

window.store = store;

export default store;
