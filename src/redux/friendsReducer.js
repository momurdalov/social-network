let initialState = {
  friends: [
    {
      id: 1,
      name: "John Wick",
      image:
        "https://i.pinimg.com/originals/f8/39/7f/f8397fa106e0cbb1c47becf65e935008.png",
    },
    {
      id: 2,
      name: "Peter Parker",
      image:
        "https://cdn.vox-cdn.com/thumbor/pTa_yD0mbfDWi7AeAOA7Ym7R4qE=/1400x1400/filters:format(jpeg)/cdn.vox-cdn.com/uploads/chorus_asset/file/23102001/DF_12080_rv2_1000x563_thumbnail.jpg",
    },
    {
      id: 3,
      name: "Henry Cavill",
      image: "https://overclockers.ru/st/legacy/blog/127193/189904_O.jpg",
    },
  ],
};

const friendsReducer = (state = initialState, action) => {
  return state;
};
export default friendsReducer;
