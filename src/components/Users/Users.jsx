import React from "react";
import styles from "./Users.module.css";
import { NavLink } from "react-router-dom";
import * as axios from "axios";

const Users = (props) => {
  let pagesCount = props.totalUsersCount / props.pageSize;

  let pages = [];

  for (let i = 1; i <= pagesCount; i++) {
    pages.push(i);
  }
  return (
    <div className={styles.wrapper}>
      <div className={styles.paginationCont}>
        {pages.map((p) => {
          return (
            <div
              onClick={() => {
                props.onPageChanged(p);
              }}
              className={
                props.currentPage === p
                  ? styles.selectedPage
                  : styles.pagination
              }
            >
              {p}
            </div>
          );
        })}
      </div>
      {props.users.map((u) => (
        <div key={u.id} className={styles.user}>
          <div>
            <span>
              <div className={styles.imageCont}>
                <NavLink to={"/profile/" + u.id}>
                  <img
                    src={
                      u.photos.small != null
                        ? u.photos.small
                        : "https://avatars.mds.yandex.net/get-kinopoisk-image/1777765/dc1c5a98-300a-486e-ad41-e3c5d1a53e25/220x330"
                    }
                    className={styles.userPhoto}
                    alt=""
                  />
                </NavLink>
              </div>
              <div>
                {u.followed ? (
                  <button
                    disabled={props.followingInProgress.some(
                      (id) => id === u.id
                    )}
                    onClick={() => {
                      props.toggleFollowingInProgress(true, u.id);
                      axios
                        .delete(
                          `https://social-network.samuraijs.com/api/1.0/follow/${u.id}`,
                          {
                            withCredentials: true,
                            headers: {
                              "API-KEY": "07572cd8-32e2-45c9-8932-7fb153c13e50",
                            },
                          }
                        )
                        .then((response) => {
                          if (response.data.resultCode === 0) {
                            props.unFollow(u.id);
                          }
                          props.toggleFollowingInProgress(false, u.id);
                        });
                    }}
                  >
                    UNFOLLOW
                  </button>
                ) : (
                  <button
                    disabled={props.followingInProgress.some(
                      (id) => id === u.id
                    )}
                    onClick={() => {
                      props.toggleFollowingInProgress(true, u.id);
                      axios
                        .post(
                          `https://social-network.samuraijs.com/api/1.0/follow/${u.id}`,
                          {},
                          {
                            withCredentials: true,
                            headers: {
                              "API-KEY": "07572cd8-32e2-45c9-8932-7fb153c13e50",
                            },
                          }
                        )
                        .then((response) => {
                          if (response.data.resultCode === 0) {
                            props.follow(u.id);
                          }
                          props.toggleFollowingInProgress(false, u.id);
                        });
                    }}
                  >
                    FOLLOW
                  </button>
                )}
              </div>
            </span>
          </div>
          <span>
            <span>
              <div>{u.name}</div>
              <div>{u.status}</div>
            </span>
            <span>
              <div>"u.location.city,u.location.country"</div>
            </span>
          </span>
        </div>
      ))}
    </div>
  );
};

export default Users;
