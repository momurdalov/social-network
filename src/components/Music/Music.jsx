import styles from "./Music.module.css";

const Music = (props) => {
  return (
    <div className={styles.wrapper}>
      <div className={styles.container}>Music</div>
    </div>
  );
};

export default Music;
