import styles from "./Settings.module.css";

const Settings = (props) => {
  return (
    <div className={styles.wrapper}>
      <div className={styles.container}>Settings</div>
    </div>
  );
};

export default Settings;
