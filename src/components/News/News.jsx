import styles from "./News.module.css";

const News = (props) => {
  return (
    <div className={styles.wrapper}>
      <div className={styles.container}>News</div>
    </div>
  );
};

export default News;
