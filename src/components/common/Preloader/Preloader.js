import React from "react";
import preloader from "../../../assets/Spinner-1s-200px.svg";

const Preloader = () => {
  return (
    <div style={{ backgroundColor: "white" }}>
      <img src={preloader} alt="" />
    </div>
  );
};

export default Preloader;
