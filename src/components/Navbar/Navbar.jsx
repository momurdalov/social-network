import React from "react";
import { NavLink } from "react-router-dom";
import classes from "./Navbar.module.css";

const Navbar = (props) => {
  return (
    <nav className={classes.nav}>
      <div className={classes.item}>
        <NavLink
          to="/profile"
          className={({ isActive }) =>
            isActive ? classes.active : classes.inactive
          }
        >
          Profile
        </NavLink>
      </div>
      <div className={classes.item}>
        <NavLink
          to="/dialogs"
          className={({ isActive }) =>
            isActive ? classes.active : classes.inactive
          }
        >
          Messages
        </NavLink>
      </div>
      <div className={classes.item}>
        <NavLink
          to="/users"
          className={({ isActive }) =>
            isActive ? classes.active : classes.inactive
          }
        >
          Users
        </NavLink>
      </div>
      <div className={classes.item}>
        <NavLink
          to="/news"
          className={({ isActive }) =>
            isActive ? classes.active : classes.inactive
          }
        >
          News
        </NavLink>
      </div>
      <div className={classes.item}>
        <NavLink
          to="/music"
          className={({ isActive }) =>
            isActive ? classes.active : classes.inactive
          }
        >
          Music
        </NavLink>
      </div>
      <div className={classes.item}>
        <NavLink
          to="/settings"
          className={({ isActive }) =>
            isActive ? classes.active : classes.inactive
          }
        >
          Settings
        </NavLink>
      </div>
      {/* <div className={classes.friendsItem}>
        <Friends friends={props.state.friends} />
      </div> */}
    </nav>
  );
};

export default Navbar;
