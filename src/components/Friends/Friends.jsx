import FriendItem from "./FriendItem/FriendItem";
import styles from "./Friends.module.css";

const Friends = (props) => {
  let state = props.friendsPage;

  let friendsList = state.friends.map((f) => (
    <FriendItem name={f.name} image={f.image} />
  ));

  return (
    <div className={styles.content}>
      <div className={styles.title}>Friends:</div>
      <div className={styles.itemsCont}>{friendsList}</div>
    </div>
  );
};

export default Friends;
