import styles from "./FriendItem.module.css";

const FriendItem = (props) => {
  return (
    <div className={styles.content}>
      <div>
        <img src={props.image} className={styles.image} alt="" />
      </div>
      {props.name}
    </div>
  );
};

export default FriendItem;
