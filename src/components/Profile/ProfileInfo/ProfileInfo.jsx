import Preloader from "../../common/Preloader/Preloader";
import styles from "./ProfileInfo.module.css";

const ProfileInfo = (props) => {
  if (!props.profile) {
    return <Preloader />;
  }

  return (
    <div className={styles.content}>
      <div>
        <img
          src="https://v-georgia.com/wp-content/uploads/2017/08/%D0%9F%D0%BB%D1%8F%D0%B6%D0%91%D0%B0%D1%82%D1%83%D0%BC%D0%B86%D0%97.jpg"
          alt=""
        />
      </div>
      <div className={styles.userCont}>
        <img
          src={
            props.profile.photos.large != null
              ? props.profile.photos.large
              : "https://avatars.mds.yandex.net/get-kinopoisk-image/1777765/dc1c5a98-300a-486e-ad41-e3c5d1a53e25/220x330"
          }
          alt=""
          className={styles.profileImage}
        />
        <div>
          <div>{props.profile.fullName}</div>
          <div>Статус:{props.profile.aboutMe}</div>
        </div>
      </div>
    </div>
  );
};

export default ProfileInfo;
