import React from "react";
import styles from "./Post.module.css";

const Post = (props) => {
  return (
    <div className={styles.posts}>
      <div className={styles.item}>
        <img
          src="https://i.pinimg.com/236x/a9/bb/b4/a9bbb43d833e264c66fce9f8d62601bb.jpg"
          alt=""
        />
        {props.message}
        <div>
          <span>likes {props.likesCount}</span>
        </div>
      </div>
    </div>
  );
};

export default Post;
